from django.apps import AppConfig


class OpenapiDjangoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'openapi_django'
