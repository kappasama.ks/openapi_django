#CHANGELOG
## [UNRELEASED]

---

## [1.4.1]
### Misc
- fix empty description

## [1.4.0] - 2022-10-19
### Misc
- up version

## [1.3.1] - 2022-10-19
### Misc
- up version

## [1.3.0] - 2022-10-19
### Misc
- micro fix tags
### Featured
- add test server schema to settings

## [1.2.0] - 2022-10-15
### Featured
- add tags

## [1.1.3] - 2022-08-14
### Misc
- add answer_content parameter info for readme.md

## [1.1.0] - 2022-08-14
### Featured
- add answer_content parameter

## [1.0.0] - 2022-06-17
### Featured
- Release
